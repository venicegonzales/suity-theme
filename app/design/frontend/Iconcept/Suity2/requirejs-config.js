var config = {

	map: {
		'theme' : 'js/theme',
		'jqueryFlexisel' : 'js/jquery.flexisel',
		'jqueryEtalage' : 'js/jquery.jquery.etalage',
		'classie1' : 'js/classie1',
	},
	shim: {
		'js/jquery-1.11.1.min' 	: ['jquery'],
	}

}