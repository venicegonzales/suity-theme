require([
	'jquery',
	'js/jquery.flexisel',
	'js/jquery.etalage.min'
], function($, flexisel) {

     $('#activator').click(function(){
			$('#box').animate({'left':'0px'},500);
	});
	$('#boxclose').click(function(){
			$('#box').animate({'left':'-2300px'},500);
	});


	//Hide (Collapse) the toggle containers on load
	$(".toggle_container").hide();

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(".trigger").click(function(){
		$(this).toggleClass("active").next().slideToggle("slow");
		return false; //Prevent the browser jump to the link anchor
	});

	$('.minicart-wrapper.header-bottom-right > ul > li').hover(
		function() {
			if( !$('a.action.showcart', $(this)).hasClass('active') ) {
				$('a.action.showcart', $(this)).trigger('click');
			}
		}
	);

	$("#flexiselDemo3").flexisel({
	    visibleItems: 3,
	    animationSpeed: 1500,
	    autoPlay: true,
	    autoPlaySpeed: 3000,
	    pauseOnHover: true,
	    enableResponsiveBreakpoints: true,
	    responsiveBreakpoints: {
	        portrait: {
	            changePoint:480,
	            visibleItems: 1
	        },
	        landscape: {
	            changePoint:640,
	            visibleItems: 2
	        },
	        tablet: {
	            changePoint:768,
	            visibleItems: 3
	        }
	    }
	});

	$("#flexiselDemo4").flexisel({
	    visibleItems: 3,
	    animationSpeed: 1000,
	    autoPlay: true,
	    autoPlaySpeed: 4000,
	    pauseOnHover: true,
	    enableResponsiveBreakpoints: true,
	    responsiveBreakpoints: {
	        portrait: {
	            changePoint:480,
	            visibleItems: 1
	        },
	        landscape: {
	            changePoint:640,
	            visibleItems: 2
	        },
	        tablet: {
	            changePoint:768,
	            visibleItems: 3
	        }
	    }
	});

	$(document).on('click', '.cbp-vm-options > a', function() {
		var view = $(this).data('view');

		if( !$(this).hasClass('cbp-vm-selected') ) {

			$(this).parent().find('.cbp-vm-selected').removeClass('cbp-vm-selected');
			$(this).addClass('cbp-vm-selected');

			if( view == 'cbp-vm-view-grid' ) {

				$('#cbp-vm').removeClass('cbp-vm-view-list').addClass('cbp-vm-view-grid');
			} else {

				$('#cbp-vm').removeClass('cbp-vm-view-grid').addClass('cbp-vm-view-list');
			}
		}

		return false;
	});
	$('#etalage').etalage({
		thumb_image_width: 300,
		thumb_image_height: 400,
		
		show_hint: true,
		click_callback: function(image_anchor, instance_id){
			alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
		}
	});
});